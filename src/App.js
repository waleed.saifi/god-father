import React from "react";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Home from "./pages/home";
import Login from "./pages/login";
import Dashboard from "./pages/dashboard";
import Inventory from "./pages/inventory";
import Jobs from "./pages/jobs";
import JobsDashboard from "./pages/jobsDashboard";
import CharacterCreation from "./pages/characterCreation";
import Cities from "./pages/cities";
import NoGang from "./pages/no-Gang";
import GangDashboard from "./pages/gangDashboard";
import Rent from "./pages/rent";
import RentHotel from "./pages/rentHotel";
import Market from "./pages/market";
import MarketUnderground from "./pages/marketUnderground";
import School from "./pages/school";
import Chanel from "./pages/chanel";

const App = () => {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/inventory" element={<Inventory />} />
        <Route path="/jobs" element={<Jobs />} />
        <Route path="/jobs-dashboard" element={<JobsDashboard />} />
        <Route path="/no-gang" element={<NoGang />} />
        <Route path="/gang-dashboard" element={<GangDashboard />} />
        <Route path="/rent" element={<Rent />} />
        <Route path="/rent-hotel" element={<RentHotel />} />
        <Route path="/market" element={<Market />} />
        <Route path="/market-underground" element={<MarketUnderground />} />
        <Route path="/school" element={<School />} />
        <Route path="/character-creation" element={<CharacterCreation />} />
        <Route path="/cities" element={<Cities />} />
        <Route path="/chanel" element={<Chanel />} />
      </Routes>
    </Router>
  );
};

export default App;
