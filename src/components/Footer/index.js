import React from "react";
import { Grid } from "@mui/material";

import "./index.css";

const Footer = () => {
  return (
    <div className="home_footer">
      <Grid container className="home_footer_container">
        <Grid item xs={12} sm={6} md={4} lg={4} xl={4}>
          <h3 className="home_footer_logo">
            The
            <br />
            Godfather
          </h3>
        </Grid>
        <Grid
          item
          xs={12}
          sm={6}
          md={4}
          lg={4}
          xl={4}
          className="home_footer_links_sec"
        >
          <p to="/" className="home_footer_link">
            About
          </p>
          <p to="/" className="home_footer_link">
            Support
          </p>
        </Grid>
        <Grid
          item
          xs={12}
          sm={12}
          md={4}
          lg={4}
          xl={4}
          className="home_footer_icons_section"
        >
          <div className="home_footer_icons" />
          <div className="home_footer_icons" />
          <div className="home_footer_icons" />
        </Grid>
      </Grid>
    </div>
  );
};
export default Footer;
