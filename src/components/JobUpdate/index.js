import React from "react";

import { useNavigate } from "react-router-dom";

import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

import "./index.css";

const JobUpdate = () => {
  const navigate = useNavigate();
  const buttonArray = ["1", "2", "3", "4"];
  const tabArray = [
    { name: "Leaderboard", link: "#" },
    { name: "Inventory", link: "/inventory" },
    { name: "Jobs", link: "/jobs" },
    { name: "Gang", link: "/no-gang" },
    { name: "Rent", link: "/rent" },
    { name: "School", link: "/school" },
    { name: "Market", link: "/market" },
    { name: "Cities", link: "/cities" },
  ];

  return (
    <>
      <Box className="dashboard_nav_heading">
        <Typography variant="h5"> News/Update</Typography>
      </Box>

      <Box className="character_info_tabs">
        <Typography variant="h5"> Character Information</Typography>
        {buttonArray.map((x) => (
          <Button className="tab_button">Tab</Button>
        ))}
      </Box>
      <Box className="dashboard_navbar">
        {tabArray.map((x) => (
          <Typography
            variant="p"
            className="dashboard_navlinks"
            onClick={() => navigate(x.link)}
          >
            {x.name}
          </Typography>
        ))}
      </Box>
    </>
  );
};
export default JobUpdate;
