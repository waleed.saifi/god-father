import React from "react";
import "./index.css";

const Features = (props) => {
  return (
    <div className="feature_comp">
      <div className="features_image" />
      <div className="feature_comp_text_section">
        <h4 className="feature_comp_title">{props.title}</h4>
        <p className="feature_comp_text">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla non
          vestibulum at scelerisque. Nunc, euismod duis sapien amet lorem. Vel
          sapien tellus orci mattis.
        </p>
      </div>
    </div>
  );
};
export default Features;
