import React from "react";
import { Grid, Typography, Box, Button, Container } from "@mui/material";
import "./index.css";
const SchoolBoxes = (props) => {
  return (
    <>
      <Box className="school-box2">
        <Grid lg={12} className="school-box-heading">
          <Grid lg={6} md={6} sm={12} xs={12}>
            <Typography className="school-heading">School 1</Typography>
          </Grid>
          <Grid lg={6} md={6} sm={12} xs={12}>
            <Box className="enroll-box">
              <Typography className="school-enroll-typhography">
                {props.Enroll}
              </Typography>
              <Typography className="school-enroll-paragraph">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Justo
                in turpis convallis facilisis id.
              </Typography>
            </Box>
          </Grid>
        </Grid>
        <div className="table">
          <Grid container lg={12} className="course-deatial">
            <Grid item lg={3} md={3} sm={3} xs={3}>
              <Typography className="school-course">Course</Typography>
            </Grid>
            <Grid item lg={4} md={4} sm={4} xs={4}>
              <Typography className="school-description">
                Description
              </Typography>
            </Grid>
            <Grid item lg={3} md={3} sm={3} xm={3}>
              <Typography className="school-cost">Cost</Typography>
            </Grid>
          </Grid>

          {props.enrollData.map((x) => (
            <Grid item lg={12} md={12}>
              <Box className="school-enroll-box">
                <Grid
                  container
                  style={{ paddingTop: "11px", paddingLeft: "3%" }}
                >
                  <Grid lg={3} md={3} ms={3} sm={3} xs={3}>
                    <Typography className="course-name">{x.course}</Typography>
                  </Grid>
                  <Grid item lg={4} md={4} sm={4} xs={4}>
                    <Typography className="course-description">
                      {x.description}
                    </Typography>
                  </Grid>
                  <Grid item lg={2} md={2} sm={2} xs={2}>
                    <Typography className="course-cost">{x.cost}</Typography>
                  </Grid>
                  <Grid
                    item
                    lg={3}
                    md={2}
                    sm={3}
                    xs={3}
                    className="enroll-button-grid"
                  >
                    <Button className="enroll-button">{x.enrollButton}</Button>
                  </Grid>
                </Grid>
              </Box>
            </Grid>
          ))}
        </div>
      </Box>
    </>
  );
};
export default SchoolBoxes;
