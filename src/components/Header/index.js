import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
// import Button from "@mui/material/Button";
import MenuItem from "@mui/material/MenuItem";

import "./index.css";

const pages = ["Story", "Characters", "Features", "Whitepaper"];

const Header = () => {
  const [anchorElNav, setAnchorElNav] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <AppBar
      position="static"
      sx={{
        background: "#6a6a6a",
        maxWidth: "1240px !important",
        margin: "auto",
        height: { lg: "80px" },
        clipPath: { md: "polygon(0% 0%, 100% 0%, 96% 100%, 4% 100%)" },
      }}
    >
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{
              flexGrow: 1,
              display: { xs: "flex", md: "none" },
              textTransform: "capitalize",
            }}
          >
            LOGO
          </Typography>

          <Box
            sx={{
              flexGrow: 1,
              display: { xs: "flex", md: "none" },
              justifyContent: { xs: "flex-end" },
            }}
          >
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              {pages.map((page) => (
                <MenuItem key={page} onClick={handleCloseNavMenu}>
                  <Typography textAlign="center">{page}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>

          <Box
            sx={{
              flexGrow: 1,
              display: { xs: "none", md: "flex" },
              justifyContent: "center",
            }}
          >
            <div
              onClick={handleCloseNavMenu}
              className="header_items"
              style={{ marginRight: "72px" }}
            >
              Story
            </div>

            <div
              onClick={handleCloseNavMenu}
              className="header_items"
              style={{ marginRight: "90px" }}
            >
              Characters
            </div>

            <div>
              <Box
                sx={{
                  display: { xs: "none", md: "flex" },
                  marginRight: "50px",
                  textTransform: "capitalize",
                }}
              >
                Logo
              </Box>
            </div>

            <div
              onClick={handleCloseNavMenu}
              className="header_items"
              style={{ marginRight: "72px" }}
            >
              Features
            </div>

            <div onClick={handleCloseNavMenu} className="header_items">
              Whitepaper
            </div>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default Header;
