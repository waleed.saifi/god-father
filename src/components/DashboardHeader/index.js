import React from "react";
import { Button } from "@mui/material";
import NotificationIcon from "../../assets/notification-icon.png";

import "./index.css";

const DashboardHeader = (props) => {
  return (
    <div className="dasboard_header">
      <div className="dashboard_header_logo">Logo</div>
      <div className="dashboard_header_user">
        {!props.isChannel ? (
          <div className="notify_icon_div">
            <img className="dashboard_header_avatar" src={NotificationIcon} />
          </div>
        ) : (
          ""
        )}
        <p className="support">Support</p>
        {!props.isChannel ? (
          <Button className="header_user_button">ki.fg.wam</Button>
        ) : (
          <p className="support">Logout</p>
        )}
      </div>
    </div>
  );
};

export default DashboardHeader;
