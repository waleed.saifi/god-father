import React from "react";

import { Button, Grid } from "@mui/material";

import "./index.css";

const DataTableHeader = (props) => {
  return (
    <Grid container className="table_header">
      <Grid item lg={2} md={2} sm={2} xs={2}>
        <p className="table_header_headings">{props.head1}</p>
      </Grid>
      <Grid item lg={2} md={2} sm={2} xs={2}>
        <p className="table_header_headings">{props.head2}</p>
      </Grid>
      <Grid item lg={4} md={4} sm={4} xs={4}>
        <p className="table_header_headings text_align_center">{props.head3}</p>
      </Grid>
      <Grid item lg={2} md={2} sm={2} xs={2}>
        <p className="table_header_headings text_align_center">{props.head4}</p>
      </Grid>
      <Grid item lg={2} md={2} sm={2} xs={2} className="text_align_right">
        <Button className="table_settings_btn">{props.head5}</Button>
      </Grid>
    </Grid>
  );
};
export default DataTableHeader;
