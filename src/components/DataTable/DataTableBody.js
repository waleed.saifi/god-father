import React from "react";

import { Box, Button, Grid } from "@mui/material";

import "./index.css";

const DataTableBody = (props) => {
  return (
    <Grid container className="table_row">
      <Grid item lg={2} md={2} sm={2} xs={2}>
        {props?.isImage ? (
          <Box className="table_img_box"></Box>
        ) : (
          <p className="table_row_count">{props.value1}</p>
        )}
      </Grid>
      <Grid item lg={2} md={2} sm={2} xs={2}>
        <p className="table_row_title">{props.value2}</p>
      </Grid>
      <Grid item lg={4} md={4} sm={4} xs={4}>
        <p className="table_row_description text_align_center">
          {props.value3}
        </p>
      </Grid>
      <Grid item lg={2} md={2} sm={2} xs={2}>
        <p className="table_row_rewards text_align_center">
          {`$${props.value4}`}
        </p>
      </Grid>
      <Grid
        item
        lg={2}
        md={2}
        sm={2}
        xs={2}
        className="text_align_right settings_accept_section"
      >
        <Button className="table_row_settings_accept ">{props.value5}</Button>
      </Grid>
    </Grid>
  );
};
export default DataTableBody;
