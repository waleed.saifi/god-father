import React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import "./index.css";

const Footer = () => {
  return (
    <Box className="dashboard_footer">
      <Typography variant="h5"> Footer</Typography>
    </Box>
  );
};
export default Footer;
