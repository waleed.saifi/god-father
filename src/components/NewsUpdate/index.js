import React from "react";
import "./index.css";

const NewsUpdates = (props) => {
  return (
    <div className="news_comp">
      <div className="news_comp_title_section">
        <h5 className="news_comp_title">{props.title}</h5>
        <div className="news_comp_avatar" />
      </div>
      <p className="news_comp_text">
        {/* {props.text} */}
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam quis vitae
        quis volutpat, duis imperdiet viverra odio. Quisque pulvinar amet dolor
        porta pharetra. Nibh in porttitor scelerisque donec in pellentesque
        lectus euismod at. Ac dictum at.
      </p>
    </div>
  );
};

export default NewsUpdates;
