import React from "react";

import { useNavigate } from "react-router-dom";
import DashboardHeader from "../../components/DashboardHeader";
import Container from "@mui/material/Container";
import JobUpdate from "../../components/JobUpdate";
import Footer from "../../components/DashboardFooter";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

import "./index.css";

const Jobs = () => {
  const navigate = useNavigate();

  return (
    <Container maxWidth="xl" className="dashboard_container">
      <DashboardHeader />
      <JobUpdate />
      <Grid container spacing={6} className="margin-top">
        <Grid item lg={7} xs={12}>
          <Box className="event-box">
            <Typography variant="h3">Event</Typography>
          </Box>
        </Grid>
        <Grid item lg={5} xs={12}>
          <Box className="jobs-box" onClick={() => navigate("/jobs-dashboard")}>
            <Typography variant="h3">Jobs</Typography>
          </Box>
          <Box className="jobs-box">
            <Typography variant="h3">Contracts</Typography>
          </Box>
        </Grid>
      </Grid>
      <Footer />
    </Container>
  );
};
export default Jobs;
