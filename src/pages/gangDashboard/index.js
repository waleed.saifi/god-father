import React from "react";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import DashboardHeader from "../../components/DashboardHeader";
import JobUpdate from "../../components/JobUpdate";
import Footer from "../../components/DashboardFooter";
import "./index.css";

const GangDash = () => {
  const navigate = useNavigate();
  return (
    <>
      <Container maxWidth="xl" className="dashboard_container">
        <DashboardHeader />
        <JobUpdate />
        <Grid container spacing={3} className="margin-top">
          <Grid item lg={8} xs={12}>
            <Box className="war-box">
              <Typography variant="h3">Gang War</Typography>
            </Box>
          </Grid>
          <Grid item lg={4} xs={12}>
            <Box className="body-box">
              <Typography variant="h4">Body Guards</Typography>
            </Box>
            <Grid container spacing={3}>
              <Grid item lg={6} xs={12}>
                <Box className="info-box">
                  <Typography variant="h5">
                    Gang
                    <br /> Info
                  </Typography>
                </Box>
              </Grid>
              <Grid item lg={6} xs={12}>
                <Box className="setting-box">
                  <Typography variant="h5">
                    Gang
                    <br /> Settings
                  </Typography>
                </Box>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Footer />
      </Container>
    </>
  );
};
export default GangDash;
