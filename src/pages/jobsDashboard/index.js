import React, { useState } from "react";

import { Container, ToggleButton, ToggleButtonGroup } from "@mui/material";
import { useNavigate } from "react-router-dom";

import DashboardHeader from "../../components/DashboardHeader";
import JobUpdate from "../../components/JobUpdate";
import DataTableHeader from "../../components/DataTable/DataTableHeader";
import DataTableBody from "../../components/DataTable/DataTableBody";
import Footer from "../../components/DashboardFooter";

import "./index.css";

const JobsDashboard = () => {
  const navigate = useNavigate();
  const [alignment, setAlignment] = useState("Normal");
  const [jobs, setJobs] = useState([
    {
      srNo: 1,
      jobTitle: "Lorem Ipsum",
      jobDescription:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu lobortis aliquam gravida et urna ornare ullamcorper nunc odio. Quam ante lacus, in diam sed pretium ac. Id non.",
      price: "1,00,000",
    },
    {
      srNo: 2,
      jobTitle: "Lorem Ipsum",
      jobDescription:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu lobortis aliquam gravida et urna ornare ullamcorper nunc odio. Quam ante lacus, in diam sed pretium ac. Id non.",
      price: "1,00,000",
    },
    {
      srNo: 3,
      jobTitle: "Lorem Ipsum",
      jobDescription:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu lobortis aliquam gravida et urna ornare ullamcorper nunc odio. Quam ante lacus, in diam sed pretium ac. Id non.",
      price: "1,00,000",
    },
  ]);

  const handleChange = (event, newAlignment) => {
    if (newAlignment === "Gang") {
      navigate("/no-gang");
    }
    setAlignment(newAlignment);
  };
  return (
    <Container maxWidth="xl" className="dashboard_container">
      <DashboardHeader />
      <JobUpdate />
      <div className="toggle_button_section">
        <ToggleButtonGroup
          value={alignment}
          exclusive
          onChange={handleChange}
          className="toggle_button_group"
          sx={{
            ".MuiToggleButtonGroup-grouped": {
              backgroundColor: "#b1b1b1!important",
            },
            ".Mui-selected": {
              backgroundColor: "#858585 !important",
            },
          }}
        >
          <ToggleButton value="Normal">Normal</ToggleButton>
          <ToggleButton value="Gang">Gang</ToggleButton>
        </ToggleButtonGroup>
      </div>
      <div className="table__section">
        <DataTableHeader
          head1={"#"}
          head2={"Job Title"}
          head3={"Job Description"}
          head4={"Job Rewards"}
          head5={"Settings"}
        />
        {jobs?.map((j) => (
          <DataTableBody
            value1={j.srNo}
            value2={j.jobTitle}
            value3={j.jobDescription}
            value4={j.price}
            value5={"Accept"}
          />
        ))}
      </div>
      <Footer />
    </Container>
  );
};
export default JobsDashboard;
