import React from "react";

import { useNavigate } from "react-router-dom";

import { Box, Container, Grid, Typography } from "@mui/material";

import DashboardHeader from "../../components/DashboardHeader";
import JobUpdate from "../../components/JobUpdate";
import Footer from "../../components/DashboardFooter";

import "./index.css";

const Gang = () => {
  const navigate = useNavigate();
  return (
    <Container maxWidth="xl" className="dashboard_container">
      <DashboardHeader />
      <JobUpdate />
      <Grid container spacing={3} className="margin-top">
        <Grid item lg={8} xs={12}>
          <Box className="war-box">
            <Typography variant="h3">Gang War</Typography>
          </Box>
        </Grid>
        <Grid item lg={4} xs={12}>
          <Box className="body-box">
            <Typography variant="h4">Body Guards</Typography>
          </Box>
          <Box className="gang-box">
            <Typography
              variant="h4"
              onClick={() => navigate("/gang-dashboard")}
            >
              Create Gang
            </Typography>
          </Box>
        </Grid>
      </Grid>
      <Footer />
    </Container>
  );
};
export default Gang;
