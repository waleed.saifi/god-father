import React from "react";
import { useNavigate } from "react-router-dom";
import { Grid, Typography, Box, Button, Container } from "@mui/material";
import DashboardHeader from "../../components/DashboardHeader";
import JobUpdate from "../../components/JobUpdate";
import Footer from "../../components/DashboardFooter";
import "./index.css";
import SchoolBoxes from "../../components/schoolBoxes";

const School = () => {
  const navigate = useNavigate();
  const enrollData = [
    {
      course: "Lorem Ipsum",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac mauris.",
      cost: "$1,000",
      enrollButton: "Enroll",
    },
    {
      course: "Lorem Ipsum",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac mauris.",
      cost: "$1,000",
      enrollButton: "Enroll",
    },
    {
      course: "Lorem Ipsum",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac mauris.",
      cost: "$1,000",
      enrollButton: "Enroll",
    },
    {
      course: "Lorem Ipsum",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac mauris.",
      cost: "$1,000",
      enrollButton: "Enroll",
    },
    {
      course: "Lorem Ipsum",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac mauris.",
      cost: "$1,000",
      enrollButton: "Enroll",
    },
  ];
  return (
    <>
      <Container maxWidth="xl" className="dashboard_container">
        <DashboardHeader />
        <JobUpdate />
        <Box className="school-box">
          <Grid container style={{ paddingTop: "11px" }}>
            <Grid item lg={4} md={4} className="first-grid">
              <Typography className="school-typography">
                Current City:
              </Typography>
              <Typography className="school-typography1">
                Lorem Ipsum
              </Typography>
            </Grid>
            <Grid item lg={4} md={4} className="seconde-grid">
              <Typography className="school-typography">
                Description:
              </Typography>
              <Typography className="school-typography2">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. A, mi
                netus id tortor aliquam nunc aliquam facilisis aliquet. Volutpat
                habitant.
              </Typography>
            </Grid>
            <Grid itme lg={4} md={4}>
              <Button className="school-move-button">Move</Button>
            </Grid>
          </Grid>
        </Box>
        <Grid container spacing={3} style={{ marginTop: "15px" }}>
          <Grid item lg={6} sm={12} md={6} xs={12}>
            <SchoolBoxes enrollData={enrollData} Enroll="Not Enrolled" />
          </Grid>
          <Grid item lg={6} sm={12} md={6} xs={12}>
            <SchoolBoxes enrollData={enrollData} Enroll="Enrolled" />
          </Grid>
        </Grid>

        <Footer />
      </Container>
    </>
  );
};
export default School;
