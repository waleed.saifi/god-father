import * as React from "react";

import { Grid, Box, Container, TextField } from "@mui/material";

import DashboardHeader from "../../components/DashboardHeader";

import "./index.css";
const CharacterCreation = () => {
  const charaterData = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "11",
    "12",
  ];
  return (
    <>
      <Container maxWidth="xl" className="dashboard_container">
        <DashboardHeader />
        <Grid container className="character_creation_container" spacing={2}>
          <Grid
            item
            lg={5}
            md={5}
            sm={11}
            xs={11}
            className="character-creation-grid"
          >
            <Grid
              container
              spacing={3}
              style={{ marginTop: "0px", marginBottom: "50px" }}
            >
              {charaterData?.map((x) => (
                <Grid item lg={4} sm={6} md={4} xs={12}>
                  <Box className="innerGrid"></Box>
                </Grid>
              ))}
            </Grid>
          </Grid>

          <Grid item lg={7} sm={12} md={7} sm={12}>
            <Grid container className="divs">
              <Grid item lg={10} xs={12} sm={10} md={5} spacing={5}>
                <Box className="character_img"></Box>
              </Grid>
              <Grid lg={12} className="character_name_section">
                <h1 className="character_name">Character name</h1>
                <TextField
                  hiddenLabel
                  id="fullWidth"
                  variant="filled"
                  className="character_name_input"
                />
                <button className="character_start_buttton">Start</button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};
export default CharacterCreation;
