import React, { useState } from "react";

import { Box, Container, Grid } from "@mui/material";

import DashboardHeader from "../../components/DashboardHeader";
import JobUpdate from "../../components/JobUpdate";
import Footer from "../../components/DashboardFooter";

import "./index.css";

const Inventory = () => {
  const [equipped, setEquipped] = useState([
    "Box1",
    "Box2",
    "Box3",
    "Box4",
    "Box5",
    "Box6",
    "Box7",
    "Box8",
    "Box9",
    "Box10",
    "Box11",
    "Box12",
  ]);
  return (
    <Container maxWidth="xl" className="dashboard_container">
      <DashboardHeader />
      <JobUpdate />
      <div className="inventory_equipped_section">
        <h6 className="inventory_equipped_heading">Equipped</h6>
        <Container maxWidth="lg">
          <Grid container spacing={1} className="inventory_equipped_grid">
            {equipped?.map((equip) => (
              <Grid item xs={3} sm={2} md={1} lg={1} xl={1}>
                <Box className="equipped_box" />
              </Grid>
            ))}
          </Grid>
        </Container>
      </div>
      <h3 className="inventory_heading">Inventory</h3>
      <Box className="inventory_box" />
      <Footer />
    </Container>
  );
};

export default Inventory;
