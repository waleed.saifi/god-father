import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

import { Container } from "@mui/material";

import DashboardHeader from "../../components/DashboardHeader";
import JobUpdate from "../../components/JobUpdate";
import DataTableHeader from "../../components/DataTable/DataTableHeader";
import DataTableBody from "../../components/DataTable/DataTableBody";
import Footer from "../../components/DashboardFooter";

import "./index.css";

const RentHotel = () => {
  const navigate = useNavigate();

  const [hotels, setHotels] = useState([
    {
      srNo: "",
      jobTitle: "Lorem Ipsum",
      jobDescription:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate cum malesuada sed. Id ac adipiscing rhoncus dui dolor lectus. Eget ut et elit cursus. Etiam egestas maecenas sapien facilisi leo adipiscing risus mattis at. Sed enim.",
      price: "1,00,000",
    },
    {
      srNo: "",
      jobTitle: "Lorem Ipsum",
      jobDescription:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate cum malesuada sed. Id ac adipiscing rhoncus dui dolor lectus. Eget ut et elit cursus. Etiam egestas maecenas sapien facilisi leo adipiscing risus mattis at. Sed enim.",
      price: "1,00,000",
    },
    {
      srNo: "",
      jobTitle: "Lorem Ipsum",
      jobDescription:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate cum malesuada sed. Id ac adipiscing rhoncus dui dolor lectus. Eget ut et elit cursus. Etiam egestas maecenas sapien facilisi leo adipiscing risus mattis at. Sed enim.",
      price: "1,00,000",
    },
    {
      srNo: "",
      jobTitle: "Lorem Ipsum",
      jobDescription:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate cum malesuada sed. Id ac adipiscing rhoncus dui dolor lectus. Eget ut et elit cursus. Etiam egestas maecenas sapien facilisi leo adipiscing risus mattis at. Sed enim.",
      price: "1,00,000",
    },
  ]);
  return (
    <>
      <Container maxWidth="xl" className="dashboard_container">
        <DashboardHeader />
        <JobUpdate />

        <div className="hotels_lists">
          <div className="hotels_lists_heading">Underground Market</div>
        </div>
        <div className="table__section">
          <DataTableHeader
            head1={"#"}
            head2={"Job Title"}
            head3={"Job Description"}
            head4={"Job Rewards"}
            head5={"Settings"}
          />
          {hotels?.map((j) => (
            <DataTableBody
              value1={j.srNo}
              value2={j.jobTitle}
              value3={j.jobDescription}
              value4={j.price}
              value5={"Rent"}
              isImage={true}
            />
          ))}
        </div>

        <Footer />
      </Container>
    </>
  );
};
export default RentHotel;
