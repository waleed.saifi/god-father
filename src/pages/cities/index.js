import * as React from "react";
import { Grid, Typography, Box } from "@mui/material";
import Container from "@mui/material/Container";
import DashboardHeader from "../../components/DashboardHeader";
import Button from "@mui/material/Button";
import "./index.css";
import { useNavigate } from "react-router-dom";
import Footer from "../../components/DashboardFooter";
import JobUpdate from "../../components/JobUpdate";
const Cities = () => {
  const navigate = useNavigate();
  const buttonArray = ["1", "2", "3", "4"];
  const tabArray = [
    { name: "Leaderboard", link: "#" },
    { name: "Inventory", link: "/inventory" },
    { name: "Jobs", link: "/jobs" },
    { name: "Gang", link: "#" },
    { name: "Rent", link: "#" },
    { name: "School", link: "#" },
    { name: "Market", link: "#" },
    { name: "Cities", link: "#" },
  ];
  const boxData = [
    {
      City: "City",
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et nec eu convallis ullamcorper quis. Turpis fames euismod donec eu mi arcu, orci nibh mattis. Sed ac sit dui, purus tortor, nulla volutpat id. Eget tincidunt auctor elementum, adipiscing nibh at facilisis. Consectetur velit viverra consectetur",
      cost: "Cost: $120.00",
      buttonText: "Travel",
    },
    {
      City: "City",
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et nec eu convallis ullamcorper quis. Turpis fames euismod donec eu mi arcu, orci nibh mattis. Sed ac sit dui, purus tortor, nulla volutpat id. Eget tincidunt auctor elementum, adipiscing nibh at facilisis. Consectetur velit viverra consectetur",
      cost: "Cost: $120.00",
      buttonText: "Travel",
    },
    {
      City: "City",
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et nec eu convallis ullamcorper quis. Turpis fames euismod donec eu mi arcu, orci nibh mattis. Sed ac sit dui, purus tortor, nulla volutpat id. Eget tincidunt auctor elementum, adipiscing nibh at facilisis. Consectetur velit viverra consectetur",
      cost: "Cost: $120.00",
      buttonText: "Travel",
    },
  ];
  return (
    <>
      <Container maxWidth="xl" className="dashboard_container">
        <DashboardHeader />
        <JobUpdate />

        <Grid container spacing={3} className="city-boxes">
          <Grid item lg={6} md={12} sm={12} xs={12}>
            <Box className="side-box"></Box>
          </Grid>

          <Grid item lg={6} md={12} sm={12} xs={12}>
            {boxData.map((x) => (
              <Box className="side-box1">
                <Grid container>
                  <Grid item lg={6} md={6} sm={6} xs={11}>
                    <Box>
                      <Typography variant="h2" className="city-name">
                        {" "}
                        {x.City}
                      </Typography>
                      <Typography className="city-description">
                        {x.text}
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid item lg={6} className="city-cost">
                    <Typography variant="h2" className="city-price">
                      {x.cost}
                    </Typography>
                    <Button className="travel-button">{x.buttonText}</Button>
                  </Grid>
                </Grid>
              </Box>
            ))}
          </Grid>
        </Grid>
        <Footer />
      </Container>
    </>
  );
};
export default Cities;
