import React from "react";

import { useNavigate } from "react-router-dom";

import { Box, Container, Grid, Typography } from "@mui/material";

import DashboardHeader from "../../components/DashboardHeader";
import JobUpdate from "../../components/JobUpdate";
import Footer from "../../components/DashboardFooter";

import "./index.css";

const GangDash = () => {
  const navigate = useNavigate();
  return (
    <>
      <Container maxWidth="xl" className="dashboard_container">
        <DashboardHeader />
        <JobUpdate />
        <Grid container spacing={3} className="margin-top">
          <Grid item lg={8} xs={12}>
            <Box className="war-box">
              <Typography variant="h3">Mansion</Typography>
            </Box>
          </Grid>
          <Grid item lg={4} xs={12}>
            <Box className="body-box" onClick={() => navigate("/rent-hotel")}>
              <Typography variant="h4">Hotel</Typography>
            </Box>
            <Grid container spacing={3}>
              <Grid item lg={6} xs={12}>
                <Box className="info-box">
                  <Typography variant="h3">House</Typography>
                </Box>
              </Grid>
              <Grid item lg={6} xs={12}>
                <Box className="setting-box">
                  <Typography variant="h5">Add Rent</Typography>
                </Box>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Footer />
      </Container>
    </>
  );
};
export default GangDash;
