import React from "react";

import { useNavigate } from "react-router-dom";

import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";

import DashboardHeader from "../../components/DashboardHeader";
import JobUpdate from "../../components/JobUpdate";
import Footer from "../../components/DashboardFooter";

import "./index.css";

const Jobs = () => {
  const navigate = useNavigate();
  return (
    <Container maxWidth="xl" className="dashboard_container">
      <DashboardHeader />
      <JobUpdate />
      <Grid container spacing={3} className="margin-top">
        <Grid item lg={10} xs={12}>
          <Grid container spacing={3}>
            <Grid item lg={10} xs={12}>
              <Box className="Player-box">
                <Typography variant="h3">New Player Guide</Typography>
              </Box>
            </Grid>
            <Grid item lg={2} xs={12}>
              <Box className="links">Links</Box>
              <Box className="links">Links</Box>
            </Grid>

            <Grid item lg={6} xs={12}>
              {" "}
              <Box className="Updates">Updates</Box>
            </Grid>
            <Grid item lg={6} xs={12}>
              <Box className="Updates">Social Media Feeds</Box>
            </Grid>
          </Grid>
        </Grid>
        <Grid item lg={2} xs={12}>
          <Box className="links-box">
            <Typography variant="h5">Quick Links</Typography>
          </Box>
        </Grid>
      </Grid>
      <Footer />
    </Container>
  );
};
export default Jobs;
