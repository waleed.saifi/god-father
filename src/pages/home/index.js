import React, { useState } from "react";

import { useNavigate } from "react-router-dom";

import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";

import Header from "../../components/Header";
import CharactersImg from "../../assets/characters-image.png";
import Features from "../../components/Features";
import Footer from "../../components/Footer";

import "./index.css";

const Home = () => {
  const navigate = useNavigate();
  const [features, setFeatures] = useState([
    { title: "Title" },
    { title: "Title" },
    { title: "Title" },
    { title: "Title" },
    { title: "Title" },
    { title: "Title" },
    { title: "Title" },
    { title: "Title" },
  ]);

  return (
    <Container
      maxWidth="xl"
      sx={{
        background: "#9a9a9a",
        paddingLeft: "0px!important",
        paddingRight: "0px!important",
      }}
    >
      <Header />
      <div className="hero-image">
        <div className="hero-text-section">
          <p className="hero-text">The Godfather</p>
          <h3 className="hero-heading">Reclaim the empire</h3>
          <p className="hero-description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Gravida
            ultricies consequat volutpat augue at interdum scelerisque pretium.
            Lectus enim eget consectetur placerat ut purus. Feugiat diam
            suspendisse accumsan, arcu tristique consequat dui. Vitae nulla
            pellentesque iaculis lorem mi sit tempor facilisis sit. Purus quis
            enim quis ut nec. Elementum in at morbi tincidunt in elementum non
            laoreet. Nunc, eros purus sit dictumst consequat varius. Magna.
          </p>
          <div className="hero-btns-section">
            <Button
              variant="filled"
              className="play-now-button"
              onClick={() => navigate("/login")}
            >
              Play Now
            </Button>
            <Button variant="outlined" className="buy-nft-button">
              Buy NFT
            </Button>
          </div>
        </div>
      </div>

      <Grid container>
        <Grid item xs={12} className="story_overview_section">
          <div className="story_overview_img_section">
            <div className="story_overview_image" />
            <h3 className="story_overview_heading">Story and Overview</h3>
          </div>
          <p className="story_overview_text">
            This story began back in 2030, when the world erupted in wars and
            governments were destroyed one by one. Through the paid and
            bloodshed one leader stood tall and proud. He brought people
            together with fear and control. The Godfather built the strongest
            gang and took over the country by force, removing cities, and
            changing the way people lived. There are only a few cities left with
            an economy built around fighting and controlling the streets.
            <br />
            <br />
            You begin your story in one of four cities located at the edges of
            the United States. It’s the year 2060 and the country is still full
            of fighting, the streets filled with drugs, and the world controlled
            by gangs. The once notorious leader is starting to get old, his
            empire unsure if he is fit to lead them as it is disbanded into 4
            major cities. Each in control of some unique manufacturers for
            weapons. With no clear leader, will you reclaim the empire and take
            the title of Godfather?
          </p>

          <div className="characters_heading">Characters</div>
          <Grid container>
            <Grid
              item
              xs={12}
              sm={12}
              md={12}
              lg={6}
              xl={6}
              sx={{ textAlign: "center" }}
            >
              <img
                src={CharactersImg}
                alt="Characters"
                className="characters_image"
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={6} xl={6}>
              <p className="characters_text">
                There are two types of characters in this game. Real users and
                NPCs. You can only create 1 real player but there won’t be a
                hard ban system in place if you decide to make multiple
                characters. The game is set up so it is most beneficial to
                create one and stick with them the whole time. NPCs will appear
                in various parts of the game like the people who hire you or
                teach you new skills, they could also be the ones who sell you
                items.
                <br />
                <br /> Each new player starts the game out with nothing but the
                shirt on their back being dropped off in a random city. They can
                view their inventory which has unlimited space. They can equip a
                number of items at once which include but are not limited to:
                weapon, food item, drink, cosmetic shirt, cosmetic hat, cosmetic
                pants, house. The player can also have an unlimited amount of
                body guards which is described in a later section. They will
                also have a reputation score, level, and amount of experience
                points needed to get to the next level. There is also a badges
                section which is achievements earned while playing. Along with
                those each user will have 2 stats, health, and energy.
              </p>
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <div style={{ background: "#929292" }}>
        <Container maxWidth="lg">
          <Grid container className="conquer_cities_section">
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <h3 className="conquer_cities_heading">
                Conquer the four
                <br /> cities now!{" "}
              </h3>
              <p className="conquer_cities_text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Dignissim sed non tellus blandit maecenas nec nunc, ut. Id a
                amet, pellentesque ut luctus tortor. Nunc massa pulvinar lorem
                nisl, massa cursus pellentesque. Porttitor consequat semper sed
                id quis adipiscing. Sem elit urna, morbi at diam sagittis,
                sagittis. Lectus libero sed nisi, massa egestas. Tincidunt
                elementum.
              </p>
            </Grid>
            <Grid
              item
              xs={12}
              sm={12}
              md={6}
              lg={6}
              className="conquer_cities_btns_section"
            >
              <Button
                variant="filled"
                className="play-now-button conquer_play_btn"
              >
                Play Now
              </Button>
              <Button
                variant="outlined"
                className="buy-nft-button conquer_buy_btn"
              >
                Buy NFT
              </Button>
            </Grid>
          </Grid>
        </Container>
      </div>
      <div className="features_section">
        <h3 className="features_heading">Features</h3>
        <Grid container spacing={4}>
          {features?.map((feature) => (
            <Grid item xs={12} sm={6} md={6} lg={3} xl={3}>
              <Features title={feature.title} />
            </Grid>
          ))}
        </Grid>
      </div>
      <div className="home_doc_section">
        <h3 className="home_doc_heading">
          Check out our game design <br /> document and learn more!
        </h3>
        <Button variant="outlined" className="buy-nft-button conquer_buy_btn">
          Document
        </Button>
      </div>
      <Footer />
    </Container>
  );
};
export default Home;
