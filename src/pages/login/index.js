import React, { useState } from "react";

import { useNavigate } from "react-router-dom";

import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";

import NewsUpdates from "../../components/NewsUpdate";

import "./index.css";

const Login = () => {
  const navigate = useNavigate();
  const [newsUpdates, setNewsUpdates] = useState([
    {
      title: "Title",
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam quis vitae quis volutpat, duis imperdiet viverra odio. Quisque pulvinar amet dolor porta pharetra. Nibh in porttitor scelerisque donec in pellentesque lectus euismod at. Ac dictum at.",
    },
    {
      title: "Title",
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam quis vitae quis volutpat, duis imperdiet viverra odio. Quisque pulvinar amet dolor porta pharetra. Nibh in porttitor scelerisque donec in pellentesque lectus euismod at. Ac dictum at.",
    },
    {
      title: "Title",
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam quis vitae quis volutpat, duis imperdiet viverra odio. Quisque pulvinar amet dolor porta pharetra. Nibh in porttitor scelerisque donec in pellentesque lectus euismod at. Ac dictum at.",
    },
  ]);
  return (
    <Container maxWidth="xl" className="login_container">
      <Grid container>
        <Grid item xs={12} md={12} lg={6} className="login_welcome">
          <h3 className="welcome_heading">
            Welcome to <br /> God Father
          </h3>
          <p className="welcome_text">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Faucibus
            felis semper duis eleifend luctus. Cras mauris, euismod mi leo, sed
            sit et aenean cursus. Nibh neque malesuada porttitor lacinia.
            Maecenas vulputate sed elit in id mauris tincidunt velit. Habitasse
            bibendum penatibus convallis in. Sodales pharetra ac ut mauris mi
            dictum lectus.
          </p>
          <Button
            variant="outlined"
            className="login_button"
            onClick={() => navigate("/dashboard")}
          >
            Login
          </Button>
        </Grid>
        <Grid item xs={12} md={12} lg={6} className="login_news_updates">
          <h3 className="news_updates_heading">News/Updates</h3>
          <section className="news_section">
            {newsUpdates?.map((news) => (
              <NewsUpdates title={news.title} text={news.text} />
            ))}
          </section>
          <section className="slider_section">
            <div className="news_updates_slider" />
            <div className="news_updates_slider" />
            <div className="news_updates_slider" />
          </section>
        </Grid>
      </Grid>
    </Container>
  );
};
export default Login;
