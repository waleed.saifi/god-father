import React from "react";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import DashboardHeader from "../../components/DashboardHeader";
import "./index.css";
import { Typography } from "@mui/material";

const Chanel = () => {
  const channelArray = [
    { channel: "Channel", tab: "Information Tab", playerstate: false },
    { channel: "Channel", tab: "Information Tab", playerstate: true },
    { channel: "Channel", tab: "Information Tab", playerstate: false },
  ];
  return (
    <>
      <Container maxWidth="xl" className="dashboard_container">
        <DashboardHeader isChannel={true} />
        {channelArray.map((x) => (
          <Box className="channel-box">
            <Grid container justifyContent="center" spacing={3}>
              <Grid item lg={4}>
                <Typography variant="h2" className="channel">
                  {x.channel}
                </Typography>
              </Grid>
              <Grid item lg={4}>
                <Typography variant="h5" className="channel-tab">
                  {x.tab}
                </Typography>
              </Grid>
              <Grid item lg={4}>
                {x.playerstate ? (
                  <Box className="play-box"> Play Now</Box>
                ) : (
                  <>
                    <Typography variant="h5" className="player-name">
                      Choose Player Name:
                    </Typography>
                    <input className="input-box" />
                    <Typography variant="h5" className="player-name">
                      City:
                    </Typography>
                    <Box className="play-box-mini"> Play Now</Box>
                  </>
                )}
              </Grid>
            </Grid>
          </Box>
        ))}
      </Container>
    </>
  );
};
export default Chanel;
